import { ApiRoutes, OrderRoutes } from "../common/enums";
import { getApiRoute, httpWrapper } from "../helpers";
import axios from "axios";


export function getOrderById(id) {
    const route = getApiRoute(ApiRoutes.ORDER, OrderRoutes.GET_BY_ID).replace(":id", id);

    return httpWrapper(axios.get(route));
}

export function deleteOrderById(id) {
    const route = getApiRoute(ApiRoutes.ORDER, OrderRoutes.DELETE_BY_ID).replace(":id", id);

    return httpWrapper(axios.delete(route));
}

export function updateOrderById(id, data) {
    const route = getApiRoute(ApiRoutes.ORDER, OrderRoutes.UPDATE_BY_ID).replace(":id", id);

    return httpWrapper(axios.put(route, data));
}

export function createOrder(data) {
    const route = getApiRoute(ApiRoutes.ORDER, OrderRoutes.CREATE);

    return httpWrapper(axios.post(route, data));
}

export function getAllOrders() {
    const route = getApiRoute(ApiRoutes.ORDER, OrderRoutes.GET_ALL);

    return httpWrapper(axios.get(route));
}