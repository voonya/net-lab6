import { ApiRoutes, DishRoutes } from "../common/enums";
import { getApiRoute, httpWrapper } from "../helpers";
import axios from "axios";


export function getDishById(id) {
    const route = getApiRoute(ApiRoutes.DISH, DishRoutes.GET_BY_ID).replace(":id", id);

    return httpWrapper(axios.get(route));
}

export function deleteDishById(id) {
    const route = getApiRoute(ApiRoutes.DISH, DishRoutes.DELETE_BY_ID).replace(":id", id);

    return httpWrapper(axios.delete(route));
}

export function updateDishById(id, data) {
    const route = getApiRoute(ApiRoutes.DISH, DishRoutes.UPDATE_BY_ID).replace(":id", id);

    return httpWrapper(axios.put(route, data));
}

export function createDish(data) {
    const route = getApiRoute(ApiRoutes.DISH, DishRoutes.CREATE);

    return httpWrapper(axios.post(route, data));
}

export function getAllDishes() {
    const route = getApiRoute(ApiRoutes.DISH, DishRoutes.GET_ALL);

    return httpWrapper(axios.get(route));
}