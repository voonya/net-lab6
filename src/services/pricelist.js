import { ApiRoutes, PriceListRoutes } from "../common/enums";
import { getApiRoute, httpWrapper } from "../helpers";
import axios from "axios";


export function getPricelistById(id) {
    const route = getApiRoute(ApiRoutes.PRICELIST, PriceListRoutes.GET_BY_ID).replace(":id", id);

    return httpWrapper(axios.get(route));
}

export function deletePricelistById(id) {
    const route = getApiRoute(ApiRoutes.PRICELIST, PriceListRoutes.DELETE_BY_ID).replace(":id", id);

    return httpWrapper(axios.delete(route));
}

export function updatePricelistById(id, data) {
    const route = getApiRoute(ApiRoutes.PRICELIST, PriceListRoutes.UPDATE_BY_ID).replace(":id", id);

    return httpWrapper(axios.put(route, data));
}

export function createPriceList(data) {
    const route = getApiRoute(ApiRoutes.PRICELIST, PriceListRoutes.CREATE);

    return httpWrapper(axios.post(route, data));
}

export function getAllPriceLists() {
    const route = getApiRoute(ApiRoutes.PRICELIST, PriceListRoutes.GET_ALL);

    return httpWrapper(axios.get(route));
}