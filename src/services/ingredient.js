import { ApiRoutes, IngredientRoutes } from "../common/enums";
import { getApiRoute, httpWrapper } from "../helpers";
import axios from "axios";


export function getIngredientById(id) {
    const route = getApiRoute(ApiRoutes.INGREDIENT, IngredientRoutes.GET_BY_ID).replace(":id", id);

    return httpWrapper(axios.get(route));
}

export function deleteIngredientById(id) {
    const route = getApiRoute(ApiRoutes.INGREDIENT, IngredientRoutes.DELETE_BY_ID).replace(":id", id);

    return httpWrapper(axios.delete(route));
}

export function updateIngredientById(id, data) {
    console.log(data);
    const route = getApiRoute(ApiRoutes.INGREDIENT, IngredientRoutes.UPDATE_BY_ID).replace(":id", id);

    return httpWrapper(axios.put(route, data));
}

export function createIngredient(data) {
    const route = getApiRoute(ApiRoutes.INGREDIENT, IngredientRoutes.CREATE);

    return httpWrapper(axios.post(route, data));
}

export function getAllIngredients() {
    const route = getApiRoute(ApiRoutes.INGREDIENT, IngredientRoutes.GET_ALL);

    return httpWrapper(axios.get(route));
}