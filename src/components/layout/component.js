import { Link } from "react-router-dom"
import { Header } from "../header";

const Layout = ({ children }) => {
    return (
        <div>
            <Header />
            <div style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>{children}</div>
        </div>
    )
}

export { Layout };


