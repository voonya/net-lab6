import { Layout } from "../layout";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { TextField, Input } from "@mui/material";
import { Button } from "@mui/material";
import { useState, useEffect, useRef } from "react";
import { getAllPriceLists, createPriceList, deletePricelistById, updatePricelistById } from "../../services";

const PriceListPage = () => {
    const [ingredients, setPriceLists] = useState([]);
    const createRef = useRef();
    const createWeightRef = useRef();
    const createPriceRef = useRef();

    const deleteRef = useRef();


    const updateIdRef = useRef();
    const updateWeightRef = useRef();
    const updatePriceRef = useRef();

    const fetchPriceLists = () => {
        getAllPriceLists().then((data) => {
            if (!data.error) {
                console.log(data);
                setPriceLists(data)
            }
        })
    }

    useEffect(fetchPriceLists, [])

    const onCreate = () => {
        const dishId = createRef.current.value
        const weight = createWeightRef.current.value
        const price = createPriceRef.current.value
        if (!dishId || !weight || !price) {
            return;
        }

        createPriceList({ dishId, weight, price }).then((data) => {
            if (!data.error) {
                fetchPriceLists();
            }
        })
    }

    const onDelete = () => {
        const id = deleteRef.current.value
        if (!id) {
            return;
        }

        deletePricelistById(Number(id)).then((data) => {
            if (!data.error) {
                fetchPriceLists();
            }
        })
    }

    const onUpdate = () => {
        const id = updateIdRef.current.value;
        const weight = updateWeightRef.current.value
        const price = updatePriceRef.current.value
        if (!id || !weight || !price) {
            return;
        }

        updatePricelistById(id, { id: Number(id), weight, price }).then((data) => {
            if (!data.error) {
                fetchPriceLists();
            }
        })
    }

    return (
        <Layout>
            <div className="formWrapper">
                <form>
                    <h3>Create</h3>
                    <TextField label="DishId" variant="filled" inputRef={createRef} />
                    <TextField label="Weight" variant="filled" inputRef={createWeightRef} />
                    <TextField label="Price" variant="filled" inputRef={createPriceRef} />
                    <Button variant="contained" onClick={onCreate}>Create</Button>
                </form>
                <form>
                    <h3>Delete</h3>
                    <TextField label="Id" variant="filled" type="number" inputRef={deleteRef} />
                    <Button variant="contained" onClick={onDelete}>Delete</Button>
                </form>
                <form>
                    <h3>Update</h3>
                    <TextField label="Id" variant="filled" type="number" inputRef={updateIdRef} />
                    <TextField label="Weight" variant="filled" inputRef={updateWeightRef} />
                    <TextField label="Price" variant="filled" inputRef={updatePriceRef} />
                    <Button variant="contained" onClick={onUpdate}>Update</Button>
                </form>
            </div>

            <h2>All pricelists:</h2>
            <TableContainer component={Paper} sx={{ maxWidth: 1000 }}>
                <Table aria-label="simple table" >
                    <TableHead>
                        <TableRow>
                            <TableCell>Id</TableCell>
                            <TableCell align="left">Dish</TableCell>
                            <TableCell align="left">Ingredients</TableCell>
                            <TableCell align="left">Weight</TableCell>
                            <TableCell align="left">Price</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {ingredients.map((row) => (
                            <TableRow
                                key={row.id}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="left">{`${row.dish.name}(${row.dish.id})`}</TableCell>
                                <TableCell align="left">{row.dish.ingredients.map(el => `${el.name}(${el.id})`).join(',')}</TableCell>
                                <TableCell align="left">{row.weight}</TableCell>
                                <TableCell align="left">{row.price}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Layout>
    )
}

export { PriceListPage };


