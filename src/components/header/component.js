import { Link } from "react-router-dom"
import './header.css';

const Header = () => {
    return (
        <div className="header__wrapper">
            <Link to={"/ingredient"}>Ingredients</Link>
            <Link to={"/dish"}>Dishes</Link>
            <Link to={"/pricelist"}>Pricelists</Link>
            <Link to={"/order"}>Order</Link>
        </div>
    )
}

export { Header };


