import { Layout } from "../layout";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { TextField, Input } from "@mui/material";
import { Button } from "@mui/material";
import { useState, useEffect, useRef } from "react";
import { getAllOrders, createOrder, deleteOrderById, updateOrderById } from "../../services";

const OrderPage = () => {
    const [ingredients, setOrders] = useState([]);
    const deleteRef = useRef();
    const createPricelistsRef = useRef();
    const updateIdRef = useRef();
    const updatePricelistsRef = useRef();

    const fetchOrders = () => {
        getAllOrders().then((data) => {
            if (!data.error) {
                console.log(data);
                setOrders(data)
            }
        })
    }

    useEffect(fetchOrders, [])

    const onCreate = () => {
        const pricelists = createPricelistsRef.current.value
        if (!pricelists) {
            return;
        }

        createOrder({ itemsId: pricelists.split(',') }).then((data) => {
            if (!data.error) {
                fetchOrders();
            }
        })
    }

    const onDelete = () => {
        const id = deleteRef.current.value
        if (!id) {
            return;
        }

        deleteOrderById(Number(id)).then((data) => {
            if (!data.error) {
                fetchOrders();
            }
        })
    }

    const onUpdate = () => {
        const id = updateIdRef.current.value;
        const pricelists = updatePricelistsRef.current.value;
        if (!id || !pricelists) {
            return;
        }

        updateOrderById(id, { id: Number(id), itemsId: pricelists.split(',') }).then((data) => {
            if (!data.error) {
                fetchOrders();
            }
        })
    }

    return (
        <Layout>
            <div className="formWrapper">
                <form>
                    <h3>Create</h3>
                    <TextField label="PricelistsId" variant="filled" inputRef={createPricelistsRef} />
                    <Button variant="contained" onClick={onCreate}>Create</Button>
                </form>
                <form>
                    <h3>Delete</h3>
                    <TextField label="Id" variant="filled" type="number" inputRef={deleteRef} />
                    <Button variant="contained" onClick={onDelete}>Delete</Button>
                </form>
                <form>
                    <h3>Update</h3>
                    <TextField label="Id" variant="filled" type="number" inputRef={updateIdRef} />
                    <TextField label="PricelistsId" variant="filled" inputRef={updatePricelistsRef} />
                    <Button variant="contained" onClick={onUpdate}>Update</Button>
                </form>
            </div>

            <h2>All orders:</h2>
            <TableContainer component={Paper} sx={{ maxWidth: 650 }}>
                <Table aria-label="simple table" >
                    <TableHead>
                        <TableRow>
                            <TableCell>Id</TableCell>
                            <TableCell align="left">Orders</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {ingredients.map((row) => (
                            <TableRow
                                key={row.id}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="left">{row.items.map(el => (`${el.dish.name}(${el.id})`) + `; Weight: ${el.weight}; Price: ${el.price} | `)}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Layout>
    )
}

export { OrderPage };


