import { Layout } from "../layout";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { TextField, Input } from "@mui/material";
import { Button } from "@mui/material";
import { useState, useEffect, useRef } from "react";
import { getAllDishes, createDish, deleteDishById, updateDishById } from "../../services";

const DishPage = () => {
    const [ingredients, setDishs] = useState([]);
    const createRef = useRef();
    const deleteRef = useRef();
    const createIngredientsRef = useRef();
    const updateNameRef = useRef();
    const updateIdRef = useRef();
    const updateIngredientsRef = useRef();

    const fetchDishs = () => {
        getAllDishes().then((data) => {
            if (!data.error) {
                console.log(data);
                setDishs(data)
            }
        })
    }

    useEffect(fetchDishs, [])

    const onCreate = () => {
        const name = createRef.current.value
        const ingredients = createIngredientsRef.current.value
        if (!name || !ingredients) {
            return;
        }

        createDish({ name, ingredientsId: ingredients.split(',') }).then((data) => {
            if (!data.error) {
                fetchDishs();
            }
        })
    }

    const onDelete = () => {
        const id = deleteRef.current.value
        if (!id) {
            return;
        }

        deleteDishById(Number(id)).then((data) => {
            if (!data.error) {
                fetchDishs();
            }
        })
    }

    const onUpdate = () => {
        const id = updateIdRef.current.value;
        const name = updateNameRef.current.value;
        const ingredients = updateIngredientsRef.current.value;
        if (!id || !name || !ingredients) {
            return;
        }

        updateDishById(id, { id: Number(id), name, ingredientsId: ingredients.split(',') }).then((data) => {
            if (!data.error) {
                fetchDishs();
            }
        })
    }

    return (
        <Layout>
            <div className="formWrapper">
                <form>
                    <h3>Create</h3>
                    <TextField label="Name" variant="filled" inputRef={createRef} />
                    <TextField label="Ingredients" variant="filled" inputRef={createIngredientsRef} />
                    <Button variant="contained" onClick={onCreate}>Create</Button>
                </form>
                <form>
                    <h3>Delete</h3>
                    <TextField label="Id" variant="filled" type="number" inputRef={deleteRef} />
                    <Button variant="contained" onClick={onDelete}>Delete</Button>
                </form>
                <form>
                    <h3>Update</h3>
                    <TextField label="Id" variant="filled" type="number" inputRef={updateIdRef} />
                    <TextField label="Name" variant="filled" inputRef={updateNameRef} />
                    <TextField label="Ingredients" variant="filled" inputRef={updateIngredientsRef} />
                    <Button variant="contained" onClick={onUpdate}>Update</Button>
                </form>
            </div>

            <h2>All dishes:</h2>
            <TableContainer component={Paper} sx={{ maxWidth: 650 }}>
                <Table aria-label="simple table" >
                    <TableHead>
                        <TableRow>
                            <TableCell>Id</TableCell>
                            <TableCell align="left">Name</TableCell>
                            <TableCell align="left">Ingredients</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {ingredients.map((row) => (
                            <TableRow
                                key={row.id}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="left">{row.name}</TableCell>
                                <TableCell align="left">{row.ingredients.map(el => `${el.name}(${el.id})`).join(',')}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Layout>
    )
}

export { DishPage };


