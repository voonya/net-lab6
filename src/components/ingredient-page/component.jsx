import { Layout } from "../layout";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { TextField, Input } from "@mui/material";
import { Button } from "@mui/material";
import { useState, useEffect, useRef } from "react";
import { getAllIngredients, createIngredient, deleteIngredientById, updateIngredientById } from "../../services";

const IngredientPage = () => {
    const [ingredients, setIngredients] = useState([]);
    const createRef = useRef();
    const deleteRef = useRef();
    const updateNameRef = useRef();
    const updateIdRef = useRef();

    const fetchIngredients = () => {
        getAllIngredients().then((data) => {
            if (!data.error) {
                setIngredients(data)
            }
        })
    }

    useEffect(fetchIngredients, [])

    const onCreate = () => {
        const name = createRef.current.value
        if (!name) {
            return;
        }

        createIngredient({ name }).then((data) => {
            if (!data.error) {
                fetchIngredients();
            }
        })
    }

    const onDelete = () => {
        const id = deleteRef.current.value
        if (!id) {
            return;
        }

        deleteIngredientById(Number(id)).then((data) => {
            if (!data.error) {
                fetchIngredients();
            }
        })
    }

    const onUpdate = () => {
        const id = updateIdRef.current.value;
        const name = updateNameRef.current.value;
        if (!id || !name) {
            return;
        }

        updateIngredientById(id, { id: Number(id), name }).then((data) => {
            if (!data.error) {
                fetchIngredients();
            }
        })
    }

    return (
        <Layout>
            <div className="formWrapper">
                <form>
                    <h3>Create</h3>
                    <TextField label="Name" variant="filled" inputRef={createRef} />
                    <Button variant="contained" onClick={onCreate}>Create</Button>
                </form>
                <form>
                    <h3>Delete</h3>
                    <TextField label="Id" variant="filled" type="number" inputRef={deleteRef} />
                    <Button variant="contained" onClick={onDelete}>Delete</Button>
                </form>
                <form>
                    <h3>Update</h3>
                    <TextField label="Id" variant="filled" type="number" inputRef={updateIdRef} />
                    <TextField label="Name" variant="filled" inputRef={updateNameRef} />
                    <Button variant="contained" onClick={onUpdate}>Update</Button>
                </form>
            </div>

            <h2>All ingredients:</h2>
            <TableContainer component={Paper} sx={{ maxWidth: 650 }}>
                <Table aria-label="simple table" >
                    <TableHead>
                        <TableRow>
                            <TableCell>Id</TableCell>
                            <TableCell align="left">Name</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {ingredients.map((row) => (
                            <TableRow
                                key={row.id}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="left">{row.name}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </Layout>
    )
}

export { IngredientPage };


