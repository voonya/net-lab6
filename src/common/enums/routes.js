export const ApiRoute = 'http://localhost:5000';

export const ApiRoutes = {
    DISH: "/dish",
    INGREDIENT: "/ingredient",
    ORDER: "/order",
    PRICELIST: "/pricelist",
}

export const DishRoutes = {
    GET_BY_ID: "/:id",
    DELETE_BY_ID: "/:id",
    UPDATE_BY_ID: "/:id",
    GET_ALL: "/",
    CREATE: "/",
}

export const IngredientRoutes = {
    GET_BY_ID: "/:id",
    DELETE_BY_ID: "/:id",
    UPDATE_BY_ID: "/:id",
    GET_ALL: "/",
    CREATE: "/",
}

export const PriceListRoutes = {
    GET_BY_ID: "/:id",
    DELETE_BY_ID: "/:id",
    UPDATE_BY_ID: "/:id",
    GET_ALL: "/",
    CREATE: "/",
}

export const OrderRoutes = {
    GET_BY_ID: "/:id",
    DELETE_BY_ID: "/:id",
    UPDATE_BY_ID: "/:id",
    GET_ALL: "/",
    CREATE: "/",
}