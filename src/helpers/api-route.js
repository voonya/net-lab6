import { ApiRoute } from "../common/enums"

export const getApiRoute = (...routes) => ApiRoute + routes.join("");