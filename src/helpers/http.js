export async function httpWrapper(cb) {
    try {
        const res = await cb;
        console.log(res.data);
        return res.data;
    }
    catch (e) {
        return e.response.data;
    }
}