import './App.css';
import { getIngredientById } from './services'
import { Button } from '@mui/material';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { Header, IngredientPage, Layout, DishPage, PriceListPage, OrderPage } from './components';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Layout></Layout>} />
          <Route path='/ingredient' element={<IngredientPage />} />
          <Route path='/dish' element={<DishPage />} />
          <Route path='/pricelist' element={<PriceListPage />} />
          <Route path='/order' element={<OrderPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
